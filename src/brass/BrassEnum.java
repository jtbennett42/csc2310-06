package brass;

//DO THIS
//a CANAL costs 3, a RAIL costs 5, and a SECOND_RAIL costs 10
enum BrassLinkCostEnum
{
    CANAL(3), RAIL(5), SECOND_RAIL(10);
    
    private int cost;
    private BrassLinkCostEnum(int num)
    {
        setCost(num);
    }
    public int getCost()
    {
        return cost;
    }
    public void setCost(int cost)
    {
        this.cost = cost;
    }
};